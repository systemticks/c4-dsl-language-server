workspace {
  name "Minimal Test"

  views {
    systemLandscape "test-landscape" {
      title "[Landscape] Test"
      !script minimalTestScript.groovy {
        variableName "TestValue"
      }
      autolayout
    }

  }

}