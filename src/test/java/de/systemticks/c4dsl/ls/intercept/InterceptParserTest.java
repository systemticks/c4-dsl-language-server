package de.systemticks.c4dsl.ls.intercept;

import java.io.File;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.structurizr.dsl.StructurizrDslParser;
import com.structurizr.dsl.StructurizrDslParserException;
import com.structurizr.model.Person;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.view.SystemContextView;

import de.systemticks.c4dsl.ls.helper.C4TestHelper;
import static org.aspectj.lang.Aspects.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InterceptParserTest {
    
    private StructurizrDslParser parser;
    private StructurizrDslParserListener mockListener;
    private File testFile;
    private InOrder checkOrder;

    @BeforeEach
    public void setupDI() {

        testFile = new File(C4TestHelper.PATH_VALID_MODELS + "/" + "financial_risk.dsl" );

        Module testModule = new AbstractModule() {

            @Override
            protected void configure() {
                mockListener = mock(StructurizrDslParserListener.class);
                bind(StructurizrDslParserListener.class).toInstance(mockListener);
                requestInjection(aspectOf(InterceptParserAspect.class));
            }
            
        };

        Injector injector = Guice.createInjector(testModule);
        parser = injector.getInstance(StructurizrDslParser.class);

        checkOrder = inOrder(mockListener);
    }

    @Test
    void startAndEndContext() throws StructurizrDslParserException {
        
        parser.parse(testFile);

        checkOrder.verify(mockListener).onStartContext(eq(testFile), eq(1), anyInt(), eq("WorkspaceDslContext"));
        checkOrder.verify(mockListener).onStartContext(eq(testFile), eq(3), anyInt(), eq("ModelDslContext"));
        checkOrder.verify(mockListener).onEndContext(eq(testFile), eq(24), anyInt(), eq("ModelDslContext"));
        checkOrder.verify(mockListener).onStartContext(eq(testFile), eq(26), anyInt(), eq("ViewsDslContext"));
        checkOrder.verify(mockListener).onStartContext(eq(testFile), eq(28), anyInt(), eq("SystemContextViewDslContext"));
        checkOrder.verify(mockListener).onEndContext(eq(testFile), eq(31), anyInt(), eq("SystemContextViewDslContext"));
        //...
        checkOrder.verify(mockListener).onEndContext(eq(testFile), eq(66), anyInt(), eq("ViewsDslContext"));
        checkOrder.verify(mockListener).onEndContext(eq(testFile), eq(67), anyInt(), eq("WorkspaceDslContext"));

    }

    @Test
    void parsedModelElements() throws StructurizrDslParserException {
        
        parser.parse(testFile);

        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(4), eq("businessUser"), any(Person.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(5), eq("configurationUser"), any(Person.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(7), eq("financialRiskSystem"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(8), eq("tradeDataSystem"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(9), eq("referenceDataSystem"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(10), eq("referenceDataSystemV2"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(11), eq("emailSystem"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(12), eq("centralMonitoringService"), any(SoftwareSystem.class));
        checkOrder.verify(mockListener).onParsedModelElement(eq(testFile), eq(13), eq("activeDirectory"), any(SoftwareSystem.class));
    }

    @Test
    void parsedRelationships() throws StructurizrDslParserException {
        
        parser.parse(testFile);

        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(15), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(16), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(17), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(18), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(19), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(20), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(21), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(22), isNull(), any());
        checkOrder.verify(mockListener).onParsedRelationShip(eq(testFile), eq(23), isNull(), any());
    }

    @Test
    void parseColor() throws StructurizrDslParserException{

        parser.parse(testFile);

        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(35));
        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(38));
        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(42));
        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(43));
        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(49));
        checkOrder.verify(mockListener).onParsedColor(eq(testFile), eq(59));
    }

    @Test
    void parseViews() throws StructurizrDslParserException {

        parser.parse(testFile);

        checkOrder.verify(mockListener).onParsedView(eq(testFile), eq(28), any(SystemContextView.class));
    }

    @Test
    void parseInclude() throws StructurizrDslParserException {
        
        File includeFile = new File(C4TestHelper.PATH_INCLUDE_MODELS + "/" + "include-test.dsl" );

        File includedModelFile = new File(C4TestHelper.PATH_INCLUDE_MODELS + "/" + "subFolder" + "/" + "model.dsl" );
        File includedStylesFile = new File(C4TestHelper.PATH_INCLUDE_MODELS + "/" + "subFolder" + "/" + "styles.dsl" );

        parser.parse(includeFile);

        checkOrder.verify(mockListener).onInclude(eq(includeFile), eq(4), eq(includedModelFile), eq("subFolder/model.dsl"));
        checkOrder.verify(mockListener).onInclude(eq(includeFile), eq(14), eq(includedStylesFile), eq("subFolder/styles.dsl"));
    }

    @Test
    void parseInlineScript() throws StructurizrDslParserException {

        File scriptDslFile = new File(C4TestHelper.PATH_SCRIPT_MODELS + "/" + "script-groovy.dsl" );

        parser.parse(scriptDslFile);

        verify(mockListener, times(1)).onRunInlineScript(eq("groovy"), anyList());
    }

    @Test
    void parseExternalScript() throws StructurizrDslParserException {

        File scriptDslFile = new File(C4TestHelper.PATH_SCRIPT_MODELS + "/" + "script-external-groovy.dsl" );

        parser.parse(scriptDslFile);

        verify(mockListener, times(1)).onRunExternalScript(any(File.class));
    }
}
