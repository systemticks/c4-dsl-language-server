package de.systemticks.c4dsl.ls.provider;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Guice;

import de.systemticks.c4dsl.ls.helper.C4TestHelper;
import de.systemticks.c4dsl.ls.intercept.InterceptParserAspect;
import de.systemticks.c4dsl.ls.intercept.StructurizrDslParserListener;
import de.systemticks.c4dsl.ls.model.C4DocumentManager;
import static org.aspectj.lang.Aspects.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class C4ViewProviderTest {
    private C4DocumentManager documentManager;
    private C4ViewProvider viewProvider;

    @BeforeEach
    public void initialize() throws IOException, URISyntaxException {

        Module testModule = new AbstractModule() {

            @Override
            protected void configure() {                
                documentManager = new C4DocumentManager();
                bind(StructurizrDslParserListener.class).toInstance(documentManager);
                requestInjection(aspectOf(InterceptParserAspect.class));
            }
            
        };

        Guice.createInjector(testModule);

        viewProvider = new C4ViewProvider();
    }

    @Test
    void testGetView_OK() {        
        File testFile = new File(C4TestHelper.PATH_VALID_MODELS + File.separator + "amazon_web_service.dsl");
        try {
            final String resultStructurizr = viewProvider.getView("AmazonWebServicesDeployment", C4TestHelper.createDocumentFromFile(testFile, documentManager), "structurizr");
            final String resultPlantUml = viewProvider.getView("AmazonWebServicesDeployment", C4TestHelper.createDocumentFromFile(testFile, documentManager), "plantuml");
            final String resultMermaid = viewProvider.getView("AmazonWebServicesDeployment", C4TestHelper.createDocumentFromFile(testFile, documentManager), "mermaid");
            assertNotNull(resultStructurizr);
            assertNotNull(resultPlantUml);
            assertNotNull(resultMermaid);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testGetView_Fail() {
        File testFile = new File(C4TestHelper.PATH_VALID_MODELS + File.separator + "amazon_web_service.dsl");
        try {
            final String resultPlantUml = viewProvider.getView("", C4TestHelper.createDocumentFromFile(testFile, documentManager), "plantuml");
            final String resultMermaid = viewProvider.getView("", C4TestHelper.createDocumentFromFile(testFile, documentManager), "mermaid");
            assertNull(resultPlantUml);
            assertNull(resultMermaid);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
