package de.systemticks.c4dsl.ls.generator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import java.util.zip.Deflater;

import com.structurizr.Workspace;
import com.structurizr.export.AbstractDiagramExporter;

import com.structurizr.export.plantuml.C4PlantUMLExporter;
import com.structurizr.export.plantuml.StructurizrPlantUMLExporter;
import com.structurizr.util.WorkspaceUtils;
import com.structurizr.view.ComponentView;
import com.structurizr.view.ContainerView;
import com.structurizr.view.DeploymentView;
import com.structurizr.view.DynamicView;
import com.structurizr.view.SystemContextView;
import com.structurizr.view.SystemLandscapeView;
import com.structurizr.view.View;

public class C4Generator {

    private static final String TEMPLATE = "{ \"code\":\"%s\", \"mermaid\":{\"theme\":\"default\", \"securityLevel\": \"loose\"}}";

    public static String generateEncodedWorkspace(Workspace workspace) throws Exception {
        return Base64.getEncoder().encodeToString(WorkspaceUtils.toJson(workspace, false).getBytes());
    }

    public static String generateEncodedPlantUml(View view, AbstractDiagramExporter exporter) throws Exception {
        // FIXME optional might be empty
        String pumlContent = createDiagramDefinition(view, exporter).get();
        return new String(encodePlantUml(pumlContent));
    }

    public static String generateEncodedMermaid(View view, AbstractDiagramExporter exporter) throws Exception {
        // FIXME optional might be empty
        String mermaidContent = createDiagramDefinition(view, exporter).get();
        return encodeMermaid(mermaidContent);
    }

    public static Optional<String> createDiagramDefinition(View view, AbstractDiagramExporter exporter) {
        ;

        if (view instanceof ContainerView) {
            return Optional.ofNullable(exporter.export((ContainerView) view).getDefinition());
        } else if (view instanceof ComponentView) {
            return Optional.ofNullable(exporter.export((ComponentView) view).getDefinition());
        } else if (view instanceof SystemContextView) {
            return Optional.ofNullable(exporter.export((SystemContextView) view).getDefinition());
        } else if (view instanceof SystemLandscapeView) {
            return Optional.ofNullable(exporter.export((SystemLandscapeView) view).getDefinition());
        } else if (view instanceof DeploymentView) {
            return Optional.ofNullable(exporter.export((DeploymentView) view).getDefinition());
        } else if (view instanceof DynamicView) {
            return Optional.ofNullable(exporter.export((DynamicView) view).getDefinition());
        } else {
            return Optional.empty();
        }

    }

    public static AbstractDiagramExporter createDiagramExporter(String writer) {

        if (writer.equals("StructurizrPlantUMLWriter")) {
            return new StructurizrPlantUMLExporter();
        } else if (writer.equals("C4PlantUMLWriter")) {
            return new C4PlantUMLExporter();
        } else {
            return new StructurizrPlantUMLExporter();
        }
    }

    private static byte[] encodePlantUml(String pumlDefinition) throws IOException {
        return Base64.getUrlEncoder().encode(compress(pumlDefinition.getBytes()));
    }

    private static byte[] compress(byte[] source) throws IOException {
        Deflater deflater = new Deflater(Deflater.BEST_COMPRESSION);
        deflater.setInput(source);
        deflater.finish();

        byte[] buffer = new byte[2048];
        int compressedLength = deflater.deflate(buffer);
        byte[] result = new byte[compressedLength];
        System.arraycopy(buffer, 0, result, 0, compressedLength);
        return result;
    }

    private static String encodeMermaid(String mermaidDefinition) {
        String s = String.format(TEMPLATE, mermaidDefinition.replaceAll("\n", "\\\\n").replaceAll("\"", "\\\\\""));
        return Base64.getEncoder().encodeToString(s.getBytes(StandardCharsets.UTF_8));
    }
}
