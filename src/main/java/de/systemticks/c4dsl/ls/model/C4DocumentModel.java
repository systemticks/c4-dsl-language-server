package de.systemticks.c4dsl.ls.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.structurizr.Workspace;
import com.structurizr.model.Element;
import com.structurizr.model.Relationship;
import com.structurizr.view.View;

/**
 * Represents a data model for a C4 document, which is based on the Structurizr DSL.
 * This model manages the raw DSL text, parsed elements, views, relationships,
 * scopes, and references to other models.
 * It also provides utility methods to interact with and manipulate these parsed data structures.
 */
public class C4DocumentModel {

	public static final String NO_SCOPE = "UnknownScope"; 

	private String rawText;
	private Workspace workspace;
	private boolean valid;
	private static final String NEW_LINE = "\\r?\\n";
	private String lines[];
	private int scopeDepth = 0;
	
    private static final Logger logger = LoggerFactory.getLogger(C4DocumentModel.class);
    
    private Map<Integer, View> viewToLineNumber = new HashMap<>();
    private Map<Integer, C4ObjectWithContext<Element>> elementsToLineNumber = new HashMap<>();
    private Map<Integer, C4ObjectWithContext<Relationship>> relationShipsToLineNumber = new HashMap<>();
	private Map<Integer, String> includesToLineNumber = new HashMap<>();
    private List<Integer> colors = new ArrayList<>();
	private List<C4DocumentModel> referencedModels = new ArrayList<>();
	private List<C4CompletionScope> scopes = new ArrayList<>();
	private String uri;
	private boolean parsedInternally;

    /**
     * Constructs a new {@code C4DocumentModel} with the specified raw text and file path.
     *
     * @param rawText the raw DSL text.
     * @param path the path to the file.
     */
	public C4DocumentModel(String rawText, String path) {
		this(rawText, path, false);
	}

    /**
     * Constructs a new {@code C4DocumentModel} with the specified raw text, file path,
     * and a flag indicating whether it was parsed internally.
     *
     * @param rawText the raw DSL text.
     * @param path the path to the file.
     * @param parsedInternally {@code true} if parsed internally; {@code false} otherwise.
     */
	public C4DocumentModel(String rawText, String path, boolean parsedInternally) {		
		this.rawText = rawText;
		this.uri = new File(path).toURI().toString();
		lines = getRawText().split(NEW_LINE);
		this.parsedInternally = parsedInternally;
	}

    /**
     * Returns the raw DSL text.
     *
     * @return the raw DSL text.
     */
	public String getRawText() {
		return rawText;
	}

    /**
     * Returns the workspace associated with this document model.
     *
     * @return the workspace.
     */
	public Workspace getWorkspace() {		
		return workspace;
	}

    /**
     * Returns the URI of the document.
     *
     * @return the URI.
     */
	public String getUri() {
		return uri;
	}

    /**
     * Checks if the document is valid.
     *
     * @return {@code true} if the document is valid; {@code false} otherwise.
     */
	public boolean isValid() {
		return valid;
	}

    /**
     * Checks if the document was parsed internally.
     *
     * @return {@code true} if parsed internally; {@code false} otherwise.
     */
	public boolean isParsedInternally() {
		return parsedInternally;
	}

    /**
     * Sets the workspace for this document model.
     *
     * @param workspace the workspace to set.
     */
	public void setWorkspace(Workspace workspace) {
		logger.debug("setWorkspace {}", workspace.getName());
		this.workspace = workspace;
	}

    /**
     * Sets the validity of the document.
     *
     * @param valid {@code true} if the document is valid; {@code false} otherwise.
     */
	public void setValid(boolean valid) {
		logger.debug("setValid {}", valid);
		this.valid = valid;
	}

    /**
     * Returns all views mapped to their respective line numbers.
     *
     * @return a set of entries containing line numbers and corresponding views.
     */
	public Set<Entry<Integer, View>> getAllViews() {
		return viewToLineNumber.entrySet();
	}

	 /**
     * Returns the list of colors associated with the document.
     *
     * @return a list of color line numbers.
     */		
	public List<Integer> getColors() {
		return colors;
	}

    /**
     * Retrieves the view at the specified line number, if available.
     *
     * @param lineNumber the line number.
     * @return an {@code Optional} containing the view if found, otherwise empty.
     */
	public Optional<View> getViewAtLineNumber(int lineNumber) {
		return Optional.ofNullable(viewToLineNumber.get(lineNumber));
	}

    /**
     * Retrieves the element at the specified line number, if available.
     *
     * @param lineNumber the line number.
     * @return an {@code Optional} containing the element if found, otherwise empty.
     */
	public Optional<C4ObjectWithContext<Element>> getElementAtLineNumber(int lineNumber) {
		return Optional.ofNullable(elementsToLineNumber.get(lineNumber));
	}

    /**
     * Returns all elements mapped to their respective line numbers.
     *
     * @return a set of entries containing line numbers and corresponding elements.
     */
	public Set<Entry<Integer, C4ObjectWithContext<Element>>> getAllElements() {
		return elementsToLineNumber.entrySet();
	}

    /**
     * Retrieves the relationship at the specified line number, if available.
     *
     * @param lineNumber the line number.
     * @return an {@code Optional} containing the relationship if found, otherwise empty.
     */
	public Optional<C4ObjectWithContext<Relationship>> getRelationshipAtLineNumber(int lineNumber) {
		return Optional.ofNullable(relationShipsToLineNumber.get(lineNumber));
	}

    /**
     * Retrieves the include directive at the specified line number, if available.
     *
     * @param lineNumber the line number.
     * @return an {@code Optional} containing the include directive if found, otherwise empty.
     */
	public Optional<String> getIncludeAtLineNumber(int lineNumber) {
		return Optional.ofNullable(includesToLineNumber.get(lineNumber));
	}

    /**
     * Returns all relationships mapped to their respective line numbers.
     *
     * @return a set of entries containing line numbers and corresponding relationships.
     */
	public Set<Entry<Integer, C4ObjectWithContext<Relationship>>> getAllRelationships() {
		return relationShipsToLineNumber.entrySet();
	}

    /**
     * Finds all elements by their ID across all referenced models and the current model.
     *
     * @param id the ID to search for.
     * @return a list of entries containing line numbers and corresponding elements.
     */
	public List<Entry<Integer, C4ObjectWithContext<Element>>> findElementsById(String id) {

		List<Set<Entry<Integer, C4ObjectWithContext<Element>>>> allElements = 
			referencedModels.stream().map( ref -> ref.elementsToLineNumber.entrySet()).collect(Collectors.toList());

		allElements.add(elementsToLineNumber.entrySet());

		return allElements.stream().flatMap(Collection::stream).filter( entry -> entry.getValue().getObject().getId().equals(id)).toList();
	}
	
	/**
     * Returns the text of a specific line number.
     *
     * @param lineNumber the line number.
     * @return the text of the specified line or {@code null} if out of bounds.
     */
	public String getLineAt(int lineNumber) {
		return lineNumber < lines.length ? lines[lineNumber] : null;
	}

    /**
     * Adds a relationship to the model at a specific line number.
     *
     * @param lineNumber the line number.
     * @param c4ObjectWithContext the relationship to add.
     */
	public void addRelationship(int lineNumber, C4ObjectWithContext<Relationship> c4ObjectWithContext) {
		relationShipsToLineNumber.put(lineNumber, c4ObjectWithContext);
	}

    /**
     * Adds an element to the model at a specific line number.
     *
     * @param lineNumber the line number.
     * @param c4ObjectWithContext the element to add.
     */
	public void addElement(int lineNumber, C4ObjectWithContext<Element> c4ObjectWithContext) {
		elementsToLineNumber.put(lineNumber, c4ObjectWithContext);
	}

    /**
     * Adds a view to the model at a specific line number.
     *
     * @param lineNumber the line number.
     * @param view the view to add.
     */
    public void addView(int lineNumber, View view) {
		viewToLineNumber.put(lineNumber, view);
    }

    /**
     * Adds a color associated with a specific line number.
     *
     * @param lineNumber the line number.
     */
    public void addColor(int lineNumber) {
		colors.add(lineNumber);
    }

    /**
     * Adds an include directive for a file at a specific line number.
     *
     * @param lineNumber the line number.
     * @param path the path of the included file.
     */
	private void addInclude(int lineNumber, String path) {
		includesToLineNumber.put(lineNumber, path);
	}

    /**
     * Adds a referenced model and an include directive at a specific line number.
     *
     * @param referencedModel the referenced model to add.
     * @param lineNumber the line number.
     * @param path the path of the included file.
     */
    public void addReferencedModel(C4DocumentModel referencedModel, int lineNumber, String path) {
		referencedModels.add(referencedModel);
		addInclude(lineNumber, path);
    }

    /**
     * Returns the list of referenced models.
     *
     * @return a list of referenced models.
     */
	public List<C4DocumentModel> getReferencedModels() {
		return this.referencedModels;
	}
 
     /**
     * Retrieves the surrounding scope at a specific line number.
     *
     * @param lineNumber the line number.
     * @return the name of the surrounding scope.
     */
	public String getSurroundingScope(int lineNumber) {

		final int adjustedLineNumber = lineNumber + 1;

		Optional<C4CompletionScope> nearestScope = scopes.stream()
				.filter(scope -> scope.getStartsAt() < adjustedLineNumber && (scope.getEndsAt() > adjustedLineNumber || scope.getEndsAt() == C4CompletionScope.SCOPE_NOT_CLOSED))
				.sorted( Comparator.comparingInt(C4CompletionScope::getStartsAt).reversed())
				.findFirst();

		return nearestScope.map(C4CompletionScope::getName).orElse(NO_SCOPE);
	}

    /**
     * Opens a new scope at a specific line number.
     *
     * @param lineNumber the line number.
     * @param contextId the ID of the context.
     * @param contextName the name of the context.
     */
	public void openScope(int lineNumber, int contextId, String contextName) {
		scopes.add(new C4CompletionScope(contextId, contextName, lineNumber, scopeDepth++));
	}

    /**
     * Closes an existing scope at a specific line number.
     *
     * @param lineNumber the line number.
     * @param contextId the ID of the context.
     * @param contextName the name of the context.
     */
    public void closeScope(int lineNumber, int contextId, String contextName) {

		scopeDepth--;
		
		scopes.stream().filter( s -> s.getId() == contextId).findFirst()
			.ifPresentOrElse( scope -> scope.setEndsAt(lineNumber), 
							  () -> logger.error("Cannot close scope {}, {}, {}", lineNumber, contextId, contextName));
    }

    /**
     * Retrieves the nearest scope at a specific line number, excluding "CommentDslContext".
     *
     * @param lineNumber the line number.
     * @return an {@code Optional} containing the nearest scope if found, otherwise empty.
     */
	public Optional<C4CompletionScope> getNearestScope(int lineNumber) {

		final int adjustedLineNumber = lineNumber + 1;

		return scopes.stream()
				.filter(scope -> !scope.getName().equals("CommentDslContext"))
				.filter(scope -> scope.getStartsAt() <= adjustedLineNumber && (scope.getEndsAt() >= adjustedLineNumber || scope.getEndsAt() == C4CompletionScope.SCOPE_NOT_CLOSED))
				.sorted( Comparator.comparingInt(C4CompletionScope::getStartsAt).reversed())
				.findFirst();		
	}

    /**
     * Returns the raw lines of the document as a list.
     *
     * @return a list of raw lines.
     */
	public List<String> getRawLines() {
		return Arrays.asList(lines);
	}

}
