package de.systemticks.c4dsl.ls.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The {@code LineTokenizer} class provides utility methods to tokenize a given string (line) into
 * a list of tokens based on a predefined set of patterns. It can also determine the position of a
 * cursor relative to these tokens.
 */
public class LineTokenizer {
    
    public static final String TOKEN_EXPR_RELATIONSHIP = "->";
    public static final String TOKEN_EXPR_ASSIGNMENT = "=";

    private final static String TOKENIZE_PATTERN = "(#?\\w+(\\.\\w+)*)|\"([^\"]*)\"|->|=|\\{|\\*";
    private final static Pattern pattern = Pattern.compile(TOKENIZE_PATTERN);

    /**
     * Tokenizes the given input line into a list of tokens.
     * 
     * @param line the input string to be tokenized
     * @return a list of {@code LineToken} objects representing the tokens found in the input line
     */
    public List<LineToken> tokenize(String line) {

        List<LineToken> result = new ArrayList<>();
        if(C4Utils.isBlank(line)) {
            return result;
        }

        Matcher matcher = pattern.matcher(line);

        while(matcher.find()) {
            result.add(new LineToken(matcher.group(0), matcher.start(), matcher.end()));
        }

        return result;
    }


    /**
     * Determines the location of the cursor relative to a list of tokens.
     * 
     * @param tokens the list of tokens to check against
     * @param charAt the character index where the cursor is located
     * @return a {@code CursorLocation} object indicating the index of the token and the position
     *         of the cursor relative to that token
     */
    public CursorLocation cursorLocation(List<LineToken> tokens, int charAt) {

        if(tokens == null || tokens.size() == 0) {
            return new CursorLocation(-1, TokenPosition.NOT_APPLICABLE);
        }

        for(int i=0; i<tokens.size(); i++) {
            LineToken t = tokens.get(i);
            if(cursorBeforeToken(t, charAt)) {
                return new CursorLocation(i, TokenPosition.BEFORE);
            }
            if(cursorInsideToken(t, charAt)) {
                return new CursorLocation(i, TokenPosition.INSIDE);
            }
        }

        return new CursorLocation(tokens.size()-1, TokenPosition.AFTER);

    }

    /**
     * Checks if the cursor is located inside a specified token.
     * 
     * @param token the token to check
     * @param charAt the character index where the cursor is located
     * @return {@code true} if the cursor is inside the token; {@code false} otherwise
     */
    boolean cursorInsideToken(LineToken token, int charAt) {
        return (charAt > token.getStart() && charAt <= token.getEnd());
    }

    /**
     * Checks if the cursor is located after a specified token.
     * 
     * @param token the token to check
     * @param charAt the character index where the cursor is located
     * @return {@code true} if the cursor is after the token; {@code false} otherwise
     */
    boolean cursorAfterToken(LineToken token, int charAt) {
        return charAt > token.getEnd();
    }

    /**
     * Checks if the cursor is located before a specified token.
     * 
     * @param token the token to check
     * @param charAt the character index where the cursor is located
     * @return {@code true} if the cursor is before the token; {@code false} otherwise
     */
    boolean cursorBeforeToken(LineToken token, int charAt) {
        return charAt <= token.getStart();
    }

    /**
     * Checks if the cursor is located between two specified tokens.
     * 
     * @param cursor the cursor location to check
     * @param indexFrom the index of the first token
     * @param indexTo the index of the second token
     * @return {@code true} if the cursor is between the two tokens; {@code false} otherwise
     */
    public boolean isBetweenTokens(CursorLocation cursor, int indexFrom, int indexTo) {
        return (cursor.getTokenIndex() == indexFrom && cursor.getTokenPosition().equals(TokenPosition.AFTER)) || 
               (cursor.getTokenIndex() == indexTo && cursor.getTokenPosition().equals(TokenPosition.BEFORE)) ;
    }

    /**
     * Checks if the cursor is located inside a specified token.
     * 
     * @param cursor the cursor location to check
     * @param index the index of the token to check
     * @return {@code true} if the cursor is inside the token; {@code false} otherwise
     */
    public boolean isInsideToken(CursorLocation cursor, int index) {
        return cursor.getTokenIndex() == index && cursor.getTokenPosition().equals(TokenPosition.INSIDE);  
    }

    /**
     * Checks if the cursor is located before a specified token.
     * 
     * @param cursor the cursor location to check
     * @param index the index of the token to check
     * @return {@code true} if the cursor is before the token; {@code false} otherwise
     */
    public boolean isBeforeToken(CursorLocation cursor, int index) {
        return cursor.getTokenIndex() == index && cursor.getTokenPosition().equals(TokenPosition.BEFORE);
    }


    /**
     * Represents the possible positions of a cursor relative to a token.
     */
    public enum TokenPosition { BEFORE, INSIDE, AFTER, NOT_APPLICABLE };

    /**
     * Represents the location of a cursor in relation to a token list.
     */
    @Data
    @AllArgsConstructor
    public class CursorLocation {
        private int tokenIndex;
        private TokenPosition tokenPosition;
    }
}
