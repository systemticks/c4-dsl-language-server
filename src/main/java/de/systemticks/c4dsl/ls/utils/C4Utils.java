package de.systemticks.c4dsl.ls.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.structurizr.view.*;

/**
 * Utility class providing various helper methods for handling strings, files, and Structurizr views.
 */
public class C4Utils {

    public static final int NOT_FOUND_WITHIN_STRING = -1;

    public static final String RENDERER_STRUCTURIZR = "structurizr";
    public static final String RENDERER_PLANTUML = "plantuml";
    public static final String RENDERER_MERMAID = "mermaid";

    /**
     * Writes the specified content to a file. Creates any necessary parent directories.
     *
     * @param out     the file to write the content to
     * @param content the content to write
     * @throws IOException if an I/O error occurs while writing to the file
     * @throws IllegalArgumentException if the file or content is null
     */
    public static void writeContentToFile(File out, String content) throws IOException {
        if (out == null || content == null) {
            throw new IllegalArgumentException("File or content must not be null");
        }
        out.getParentFile().mkdirs();
        try (FileWriter fw = new FileWriter(out)) {
            fw.write(content);
        }
    }

    /**
     * Finds the start position of the first occurrence of a key within a string, ignoring case.
     *
     * @param line the string to search within
     * @param key  the key to search for
     * @return the start position of the key if found, or {@code NOT_FOUND_WITHIN_STRING} if not found
     */
    public static int getStartPosition(String line, String key) {
        if (isBlank(line) || isBlank(key)) {
            return NOT_FOUND_WITHIN_STRING;
        }

        Matcher matcher = Pattern.compile("\\b" + Pattern.quote(key) + "\\b", Pattern.CASE_INSENSITIVE).matcher(line);
        return matcher.find() ? matcher.start() : NOT_FOUND_WITHIN_STRING;
    }

    /**
     * Retrieves the identifier associated with a given Structurizr view.
     *
     * @param view the view to get the identifier for
     * @return the identifier for the view, or {@code null} if the view type is not recognized or is null
     */
    public static String getIdentifierOfView(View view) {
        if (view == null) {
            return null;
        }

        if( view instanceof ContainerView containerView) {
            return containerView.getSoftwareSystem().getId();
        } else if (view instanceof ComponentView componentView) {
            return componentView.getContainer().getId();
        } else if (view instanceof DynamicView dynamicView) {
            return dynamicView.getElementId();
        } else if (view instanceof ModelView modelView) {
            return modelView.getSoftwareSystemId();
        }

        return null;
    }

    /**
     * Finds the first non-whitespace character in a given character sequence starting from a specified position.
     *
     * @param line                   the character sequence to search in
     * @param startPos               the position to start searching from
     * @param treatNewLineAsWhitespace whether newline characters should be treated as whitespace
     * @return the index of the first non-whitespace character, or {@code NOT_FOUND_WITHIN_STRING} if none found
     */
    public static int findFirstNonWhitespace(CharSequence line, int startPos, boolean treatNewLineAsWhitespace) {
        if (line == null || startPos < 0 || startPos >= line.length()) {
            return NOT_FOUND_WITHIN_STRING;
        }

        for (int i = startPos; i < line.length(); i++) {
            char c = line.charAt(i);
            if (!treatNewLineAsWhitespace && (c == '\n' || c == '\r')) {
                return NOT_FOUND_WITHIN_STRING;
            }
            if (!Character.isWhitespace(c)) {
                return i;
            }
        }
        return NOT_FOUND_WITHIN_STRING;
    }

    /**
     * Checks if a given string is null or contains only whitespace characters.
     *
     * @param str the string to check
     * @return {@code true} if the string is null or contains only whitespace, {@code false} otherwise
     */
    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * Returns the substring from the start of the line to the specified cursor position, trimmed of any surrounding whitespace.
     *
     * @param line   the line to extract from
     * @param cursor the position to extract up to
     * @return an {@link Optional} containing the trimmed substring, or an empty {@link Optional} if the input is invalid
     */
    public static Optional<String> leftFromCursor(String line, int cursor) {
        if (line == null || cursor < 0) {
            return Optional.empty();
        }
        return Optional.of(line.substring(0, Math.min(cursor, line.length())).trim());
    }

    /**
     * Merges two lists into a single list. Handles cases where one or both lists may be null.
     *
     * @param <T>   the type of elements in the lists
     * @param list1 the first list
     * @param list2 the second list
     * @return a merged list containing all elements from both input lists, or an empty list if both are null
     */
    public static <T> List<T> merge(List<T> list1, List<T> list2) {
        if (list1 == null && list2 == null) {
            return Collections.emptyList();
        }
        if (list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }
        return Lists.newArrayList(Iterables.concat(list1, list2));
    }
}
