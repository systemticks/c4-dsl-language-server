package de.systemticks.c4dsl.ls.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The {@code LineToken} class represents a single token extracted from a line of text. 
 * It stores the token's string value and its position within the original text.
 */
@Data
@AllArgsConstructor
public class LineToken {
    private String token;
    private int start;
    private int end;
}