package de.systemticks.c4dsl.ls.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Map.Entry;

import org.eclipse.lsp4j.DefinitionParams;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.LocationLink;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.structurizr.model.ContainerInstance;
import com.structurizr.model.Element;
import com.structurizr.model.SoftwareSystemInstance;

import de.systemticks.c4dsl.ls.model.C4DocumentModel;
import de.systemticks.c4dsl.ls.model.C4ObjectWithContext;
import de.systemticks.c4dsl.ls.utils.C4Utils;

/**
 * Provides definition-related functionalities for a C4 DSL document in the context
 * of the Language Server Protocol (LSP).
 * 
 * This provider is responsible for calculating the definitions and locations of elements
 * within a C4 model, such as references to views, relationships, and elements.
 */
public class C4DefinitionProvider {

    private static final Logger logger = LoggerFactory.getLogger(C4DefinitionProvider.class);
	private static final String IDENTIFIER_WILDCARD = "*";

    /**
     * Calculates the definitions for a given document model and definition parameters.
     *
     * This method analyzes the document to find the locations of elements, references,
     * and relationships based on the provided cursor position and other parameters.
     *
     * @param c4Model the C4 document model representing the current document state
     * @param params the definition parameters containing information about the cursor position
     * @return an {@link Either} object containing a list of {@link Location} or {@link LocationLink}
     *         representing the found definitions
     */
	public Either<List<? extends Location>, List<? extends LocationLink>> calcDefinitions(C4DocumentModel c4Model, DefinitionParams params) {

		int currentLineNumner = params.getPosition().getLine()+1;

		List<Location> locations = new ArrayList<>();
		logger.debug("calcDefinitions for line {}", currentLineNumner);

		// search for references in views
		c4Model.getViewAtLineNumber(currentLineNumner).ifPresent( v -> {
			findModelElementById(c4Model, C4Utils.getIdentifierOfView(v), params).ifPresent( loc -> locations.add(loc));
		});

		c4Model.getRelationshipAtLineNumber(currentLineNumner).ifPresent( r -> {
			findModelElementById(c4Model, r.getObject().getSourceId(), params).ifPresent( loc -> locations.add(loc));
			findModelElementById(c4Model, r.getObject().getDestinationId(), params).ifPresent( loc -> locations.add(loc));
		});

		c4Model.getElementAtLineNumber(currentLineNumner).ifPresent( e -> {
			if(e.getObject() instanceof ContainerInstance) {
				findModelElementById(c4Model, ((ContainerInstance) e.getObject()).getContainerId(), params).ifPresent( loc -> locations.add(loc));
			}
			else if(e.getObject() instanceof SoftwareSystemInstance) {
				findModelElementById(c4Model, ((SoftwareSystemInstance) e.getObject()).getSoftwareSystemId(), params).ifPresent( loc -> locations.add(loc));
			}	
		});

		c4Model.getIncludeAtLineNumber(currentLineNumner).ifPresent( path -> {
			final int startPos = C4Utils.getStartPosition(c4Model.getLineAt(params.getPosition().getLine()), path);
			final int endPos = startPos + path.length();
			if(params.getPosition().getCharacter() >= startPos && params.getPosition().getCharacter() <= endPos) {
				C4DocumentModel ref = c4Model.getReferencedModels().stream().filter( r -> r.getUri().endsWith(path)).findFirst().get();
				final Location location = new Location();
				location.setRange(new Range( new Position(0,0), new Position(0,0)));
				location.setUri(ref.getUri());
				locations.add(location);
			}
			logger.debug("**** START_POS INCLUDE "+startPos);
		});

		return Either.forLeft(locations);
	}

    /**
     * Finds the model element by its identifier within the provided document model.
     * 
     * This method searches for an element with a specific identifier and, if found,
     * returns its location within the document.
     *
     * @param hostModel the host C4 document model to search within
     * @param id the identifier of the element to find
     * @param params the definition parameters containing information about the cursor position
     * @return an {@link Optional} containing the {@link Location} of the found element, or empty if not found
     */
	private Optional<Location> findModelElementById(C4DocumentModel hostModel, String id, DefinitionParams params) {

		if(id == null || id.equals(IDENTIFIER_WILDCARD)) {
			return Optional.empty();
		}

		Optional<Location> result = Optional.empty();
		List<Entry<Integer, C4ObjectWithContext<Element>>> refs = hostModel.findElementsById(id);
		if(refs.size() == 1) {
			C4DocumentModel refModel = refs.get(0).getValue().getContainer();
			int refLineNumber = refs.get(0).getKey();
			C4ObjectWithContext<Element> element = refModel.getElementAtLineNumber(refLineNumber).get();
			logger.debug("Found referenced element in line {} for usage in line {}", refLineNumber, params.getPosition().getLine());
			logger.debug("    Details: {}",element.getIdentifier());
			final int startPos = C4Utils.getStartPosition(hostModel.getLineAt(params.getPosition().getLine()), element.getIdentifier());
			if(startPos == C4Utils.NOT_FOUND_WITHIN_STRING) {
				logger.error("Identifier {} not found in line {} ", element.getIdentifier(), params.getPosition().getLine());
			}
			else {
				final int endPos = startPos + element.getIdentifier().length();
				if(params.getPosition().getCharacter() >= startPos && params.getPosition().getCharacter() <= endPos) {
					logger.debug("    Cursor {} within range [{}, {}]", params.getPosition().getCharacter(), startPos, endPos);
					result = Optional.of(createLocation(refModel, refLineNumber-1, element.getIdentifier()));
				}
				else {
					logger.debug("    Cursor {} out of range [{}, {}]", params.getPosition().getCharacter(), startPos, endPos);			
				}	
			}
		}

		return result;
	}

    /**
     * Creates a new {@link Location} object for a referenced element within a document model.
     *
     * This method determines the start and end positions of a reference in the document
     * and constructs a location object representing that range.
     *
     * @param c4Model the C4 document model containing the referenced element
     * @param lineNumber the line number where the reference is located
     * @param referencedId the identifier of the referenced element
     * @return a {@link Location} object representing the range of the referenced element in the document
     */
	private Location createLocation(C4DocumentModel c4Model, int lineNumber, String referencedId) {

		Location location = new Location();

		final int refStartPos = c4Model.getLineAt(lineNumber).indexOf(referencedId);
		final int refEndPos = refStartPos + referencedId.length();
		logger.debug("    Reference Found at linenumber {}, in range [{}, {}]", lineNumber, refStartPos, refEndPos);
		location.setRange(new Range( new Position(lineNumber,refStartPos), new Position(lineNumber,refEndPos)));
		location.setUri(c4Model.getUri());

		return location;
	}
	
}
