package de.systemticks.c4dsl.ls.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.lsp4j.Color;
import org.eclipse.lsp4j.ColorInformation;
import org.eclipse.lsp4j.ColorPresentation;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.systemticks.c4dsl.ls.model.C4DocumentModel;

/**
 * Provides color-related functionalities for a C4 DSL document in the context
 * of the Language Server Protocol (LSP).
 * 
 * This provider is responsible for identifying colors defined in a document
 * and generating appropriate color representations and suggestions.
 */
public class C4ColorProvider {

    private static final Logger logger = LoggerFactory.getLogger(C4ColorProvider.class);
    private static final int COLOR_STR_LENGTH = 7; // e.g. #00FF00
    private static final String COLOR_START_TOKEN = "#";

    /**
     * Calculates the color information for the entire document based on the
     * occurrences of color codes.
     *
     * This method scans the document for color codes in hexadecimal format
     * (e.g., #RRGGBB) and generates a list of {@link ColorInformation} objects
     * that represent the colors found in the document.
     *
     * @param model the C4 document model representing the current document state
     * @return a list of {@link ColorInformation} containing the color ranges and values
     */
	public List<ColorInformation> calcDocumentColors(C4DocumentModel model) {

		logger.debug("calcDocumentColors {}", model.getUri());
		logger.debug("                   {}", model.getColors().toString());
		
		try  {
			List<ColorInformation> result = model.getColors().stream().map( lineNmbr -> {
				String rawline = model.getLineAt(lineNmbr-1);
				int startPos = rawline.indexOf(COLOR_START_TOKEN);
				int endPos = startPos + COLOR_STR_LENGTH;
				Range range = new Range(new Position(lineNmbr-1, startPos), new Position(lineNmbr-1, endPos));
				return new ColorInformation(range, hexToColor(rawline.substring(startPos, endPos)));
			}).toList();
							
			return result;	
		}
		catch( Exception e) {
			logger.error("Cannot provider color information: {}, {}", e.getClass().getSimpleName(), e.getMessage());
			return new ArrayList<>();
		}

	}

    /**
     * Calculates the color presentations for a given color.
     *
     * This method converts a given color into its possible string representations,
     * typically in hexadecimal format.
     *
     * @param color the color to be converted into presentations
     * @return a list of {@link ColorPresentation} representing the color
     */
	public List<ColorPresentation> calcColorPresentations(Color color) {		
		return Collections.singletonList(new ColorPresentation(colorToHex(color)));
	}

    /**
     * Converts a hexadecimal color string into an LSP-compatible {@link Color} object.
     *
     * The input should be a string representing a color in the form "#RRGGBB".
     *
     * @param hexColor the hexadecimal color string
     * @return a {@link Color} object representing the RGB values of the color
     */
	private Color hexToColor(String hexColor) {
		java.awt.Color c = java.awt.Color.decode(hexColor);		
		return new Color( (double)c.getRed()/255, (double)c.getGreen()/255, (double)c.getBlue()/255, 1.0 );		
	}
	
	/**
     * Converts an LSP-compatible {@link Color} object into a hexadecimal color string.
     *
     * The output is a string in the format "#RRGGBB".
     *
     * @param color the {@link Color} object to be converted
     * @return a hexadecimal color string
     */
	private String colorToHex(Color color) {
		return String.format("#%02X%02X%02X", (int)(color.getRed()*255), (int)(color.getGreen()*255), (int)(color.getBlue()*255));
	}

}
