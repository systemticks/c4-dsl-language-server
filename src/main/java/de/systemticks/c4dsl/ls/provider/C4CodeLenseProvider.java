package de.systemticks.c4dsl.ls.provider;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

import com.structurizr.Workspace;
import com.structurizr.export.mermaid.MermaidDiagramExporter;
import com.structurizr.export.plantuml.StructurizrPlantUMLExporter;
import com.structurizr.view.View;

import de.systemticks.c4dsl.ls.generator.C4Generator;
import de.systemticks.c4dsl.ls.model.C4DocumentModel;
import de.systemticks.c4dsl.ls.utils.C4Utils;

/**
 * Provides code lens functionalities for a C4 DSL document in the context
 * of the Language Server Protocol (LSP).
 * 
 * This provider is responsible for generating code lenses that offer commands
 * to visualize C4 model views in different diagram formats such as Structurizr,
 * PlantUML, and Mermaid.
 */
public class C4CodeLenseProvider {

	private Map<String, BiFunction<Workspace, View, Command>> renderFunctions = Map.ofEntries(
		new AbstractMap.SimpleEntry<String, BiFunction<Workspace, View, Command>>(C4Utils.RENDERER_STRUCTURIZR, toStructurizr),
		new AbstractMap.SimpleEntry<String, BiFunction<Workspace, View, Command>>(C4Utils.RENDERER_PLANTUML, toPlantUML),
		new AbstractMap.SimpleEntry<String, BiFunction<Workspace, View, Command>>(C4Utils.RENDERER_MERMAID, toMermaid)
	  );

    /**
     * Creates a command to render the view as a Structurizr diagram.
     */
	public static BiFunction<Workspace, View, Command> toStructurizr = (workspace, view) -> {
		Command command = new Command("$(link-external) Show as Structurizr Diagram", "c4.show.diagram");
		command.setArguments(new ArrayList<Object>());
		try {
			command.getArguments().add(C4Generator.generateEncodedWorkspace(workspace));
		} catch (Exception e) {
			e.printStackTrace();
		}
		command.getArguments().add(view.getKey());
		return command;
	};

    /**
     * Creates a command to render the view as a PlantUML diagram.
     */
	public static BiFunction<Workspace, View, Command> toPlantUML = (workspace, view) -> {
		Command command = new Command("$(link-external) Show as PlantUML Diagram", "c4.show.plantuml");
		command.setArguments(new ArrayList<Object>());
		try {
			command.getArguments().add(C4Generator.generateEncodedPlantUml(view, new StructurizrPlantUMLExporter()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		command.getArguments().add(view.getKey());
		return command;
	};

    /**
     * Creates a command to render the view as a Mermaid diagram.
     */
	public static BiFunction<Workspace, View, Command> toMermaid = (workspace, view) -> {
		Command command = new Command("$(link-external) Show as Mermaid Diagram", "c4.show.mermaid");
		command.setArguments(new ArrayList<Object>());
		try {
			command.getArguments().add(C4Generator.generateEncodedMermaid(view, new MermaidDiagramExporter()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		command.getArguments().add(view.getKey());
		return command;
	};

    /**
     * Calculates the code lenses for a given C4 document model based on the specified renderer.
     * 
     * This method generates code lenses that provide commands to visualize the views in the 
     * document model using the specified rendering format (e.g., Structurizr, PlantUML, or Mermaid).
     *
     * @param c4 the C4 document model representing the current document state
     * @param renderer the name of the renderer to use for generating diagrams
     * @return a list of {@link CodeLens} objects representing the commands for rendering views
     */
	public List<CodeLens> calcCodeLenses(C4DocumentModel c4, String renderer) {
		
		BiFunction<Workspace, View, Command> func = renderFunctions.getOrDefault(renderer, toPlantUML);

		if(!c4.isValid() && c4.getWorkspace() != null) {
			return Collections.emptyList();
		}
		
		return c4.getAllViews().stream().map( entry -> {
			int lineNumber = entry.getKey();
			String line = c4.getLineAt(lineNumber-1);
			int pos = C4Utils.findFirstNonWhitespace(line, 0, true);
			Range range = new Range(new Position(lineNumber-1, pos), new Position(lineNumber-1, pos));
			return new CodeLens(range, func.apply(c4.getWorkspace(), entry.getValue()), null);				
		}).toList();
	}
}
