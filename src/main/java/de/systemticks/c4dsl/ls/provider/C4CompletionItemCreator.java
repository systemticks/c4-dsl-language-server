package de.systemticks.c4dsl.ls.provider;

import java.util.List;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.InsertTextFormat;

import de.systemticks.c4dsl.ls.model.C4TokensConfig.C4TokenSnippet;

/**
 * A helper class responsible for creating and configuring {@link CompletionItem} objects
 * for various types of completion suggestions in a C4 DSL document.
 * 
 * This class is used by the {@link C4CompletionProvider} to generate completion items
 * for keywords, properties, snippets, and identifiers.
 */
public class C4CompletionItemCreator {

    /**
     * Creates a list of {@link CompletionItem} objects for keyword completions.
     * 
     * @param keywords a list of keyword strings to be converted into completion items
     * @return a list of {@link CompletionItem} objects representing the keywords
     */
    public List<CompletionItem> keyWordCompletion(List<String> keywords) {
        return keywords.stream().map(keyword -> createCompletionItem(keyword, CompletionItemKind.Keyword))
                .toList();
    }

    /**
     * Creates a list of {@link CompletionItem} objects for property completions.
     * 
     * @param properties a list of property strings to be converted into completion items
     * @return a list of {@link CompletionItem} objects representing the properties
     */
    public List<CompletionItem> propertyCompletion(List<String> properties) {
        return properties.stream().map(prop -> createCompletionItem(prop, CompletionItemKind.Property))
                .toList();
    }

    /**
     * Creates a list of {@link CompletionItem} objects for snippet completions.
     * 
     * This method configures each snippet with its label, detail, insert text format,
     * and the actual text to be inserted.
     *
     * @param snippets a list of {@link C4TokenSnippet} objects representing code snippets
     * @return a list of {@link CompletionItem} objects representing the snippets
     */
    public List<CompletionItem> snippetCompletion(List<C4TokenSnippet> snippets) {
        return snippets.stream().map(snippet -> {
            CompletionItem item = createCompletionItem(snippet.getLabel(), CompletionItemKind.Snippet);
            item.setDetail(snippet.getDetail());
            item.setInsertTextFormat(InsertTextFormat.Snippet);
            item.setInsertText(snippet.getInsertText());
            return item;
        }).toList();
    }

    /**
     * Creates a list of {@link CompletionItem} objects for identifier completions.
     * 
     * @param identifier a list of identifier strings to be converted into completion items
     * @return a list of {@link CompletionItem} objects representing the identifiers
     */
    public List<CompletionItem> identifierCompletion(List<String> identifier) {
        return identifier.stream().map(id -> createCompletionItem(id, CompletionItemKind.Reference))
                .toList();
    }

    /**
     * Creates a {@link CompletionItem} object with the specified label and kind.
     * 
     * @param label the label of the completion item
     * @param kind the kind of the completion item, such as keyword, property, snippet, or reference
     * @return a configured {@link CompletionItem} object
     */
    private CompletionItem createCompletionItem(String label, CompletionItemKind kind) {
        CompletionItem item = new CompletionItem();
        item.setLabel(label);
        item.setKind(kind);
        return item;
    }

}
