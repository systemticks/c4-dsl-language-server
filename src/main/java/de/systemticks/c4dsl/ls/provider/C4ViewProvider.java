package de.systemticks.c4dsl.ls.provider;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.structurizr.export.mermaid.MermaidDiagramExporter;
import com.structurizr.export.plantuml.StructurizrPlantUMLExporter;
import com.structurizr.view.View;

import de.systemticks.c4dsl.ls.generator.C4Generator;
import de.systemticks.c4dsl.ls.model.C4DocumentModel;

public class C4ViewProvider {

    private static final Logger logger = LoggerFactory.getLogger(C4ViewProvider.class);

    public String getView(String viewKey, C4DocumentModel model, String renderer) {
        switch (renderer) {
            case "structurizr":
                return this.getStructurizrView(model);
            case "plantuml":
                return this.getPlantUmlView(model, viewKey);
            case "mermaid":
                return this.getMermaidView(model, viewKey);
            default:
                return this.getPlantUmlView(model, viewKey);
        }
    }

    private String getStructurizrView(C4DocumentModel model) {
        try {
            return C4Generator.generateEncodedWorkspace(model.getWorkspace());
        } catch (Exception e) {
            logger.error("Cannot generate encoded workspace: {}", e.getMessage());
            return null;
        }
    }

    private String getPlantUmlView(C4DocumentModel model, String viewKey) {
        try {
            final View view = this.extractView(model, viewKey);
            return C4Generator.generateEncodedPlantUml(view, new StructurizrPlantUMLExporter());
        } catch (Exception e) {
            logger.error("Cannot generate encoded plantuml code: {}", e.getMessage());
            return null;
        }
    }

    private String getMermaidView(C4DocumentModel model, String viewKey) {
        try {
            final View view = this.extractView(model, viewKey);
            return C4Generator.generateEncodedMermaid(view, new MermaidDiagramExporter());
        } catch (Exception e) {
            logger.error("Cannot generate encoded mermaid code: {}", e.getMessage());
            return null;
        }
    }

    private View extractView(C4DocumentModel model, String viewKey) throws Exception {
        final Entry<Integer, View> view = model
            .getAllViews()
            .stream()
            .filter(entry -> entry.getValue().getKey().equals(viewKey))
            .findFirst()
            .orElseThrow(() -> new Exception("View not found in the model."));
        return view.getValue();
    }
}
