package de.systemticks.c4dsl.ls.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.structurizr.model.Component;
import com.structurizr.model.Container;
import com.structurizr.model.ContainerInstance;
import com.structurizr.model.DeploymentNode;
import com.structurizr.model.Element;
import com.structurizr.model.InfrastructureNode;
import com.structurizr.model.Person;
import com.structurizr.model.Relationship;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.model.SoftwareSystemInstance;

import de.systemticks.c4dsl.ls.model.C4DocumentModel;
import de.systemticks.c4dsl.ls.model.C4ObjectWithContext;
import de.systemticks.c4dsl.ls.utils.LineToken;
import de.systemticks.c4dsl.ls.utils.LineTokenizer;
import lombok.Data;

/**
 * Provides text decoration functionalities for a C4 DSL document in the context
 * of the Language Server Protocol (LSP).
 * 
 * This provider is responsible for calculating text decorations (e.g., syntax highlighting, 
 * labels) for different elements and relationships within a C4 model.
 */
public class C4TextDecoratorProvider {
    
    private static final Logger logger = LoggerFactory.getLogger(C4TextDecoratorProvider.class);
    private LineTokenizer tokenizer = new LineTokenizer();

    /**
     * Calculates the text decorations for a given C4 document model.
     * 
     * This method analyzes all elements and relationships within the document model
     * to generate a list of {@link DecoratorRange} objects representing the text decorations
     * for syntax highlighting and labeling.
     *
     * @param model the C4 document model representing the current document state
     * @return a list of {@link DecoratorRange} objects representing the text decorations
     */
    public List<DecoratorRange> calculateDecorations(C4DocumentModel model) {

        List<DecoratorRange> result = new ArrayList<>();

        if(model == null || !model.isValid()) {
            logger.warn("Cannot calculate text decorations");
            return result;
        }

        model.getAllElements().forEach( entry -> {
            
            String line = model.getLineAt(lineNumber(entry.getKey()));
            C4ObjectWithContext<Element> element = entry.getValue();

            result.addAll(calculateDecoratorsForElement(element, line, lineNumber(entry.getKey())));

        });

        model.getAllRelationships().forEach( entry -> {
            String line = model.getLineAt(lineNumber(entry.getKey()));
            Relationship relationShip = entry.getValue().getObject();

            result.addAll(calculateDecoratorsForRelationship(relationShip, line, lineNumber(entry.getKey())));
        });

        return result;
    }

    /**
     * Calculates text decorations for a given relationship in the document.
     * 
     * @param relationship the {@link Relationship} object representing the relationship to decorate
     * @param line the text line containing the relationship definition
     * @param lineNumber the line number in the document where the relationship is defined
     * @return a list of {@link DecoratorRange} objects representing the text decorations for the relationship
     */
    List<DecoratorRange> calculateDecoratorsForRelationship(Relationship relationship, String line, int lineNumber) {
        return decorationsForRelationship(line, lineNumber, RELATIONSHIP_DECORATIONS);
    }
    /**
     * Generates text decorations for a specific type of relationship within a given line.
     *
     * @param line the text line containing the relationship definition
     * @param lineNumber the line number in the document where the relationship is defined
     * @param decorationLabels an array of decoration labels to be applied to the relationship
     * @return a list of {@link DecoratorRange} objects representing the decorations for the relationship
     */
    List<DecoratorRange> decorationsForRelationship(String line, int lineNumber, String[] decorationLabels) {

        List<LineToken> tokens = tokenizer.tokenize(line);
        int firstIndex = (tokens.get(1).getToken().equals(LineTokenizer.TOKEN_EXPR_RELATIONSHIP)) ? 3 : 2;

        return IntStream.range(0, decorationLabels.length)
            .filter( i -> firstIndex + i < tokens.size() && !tokens.get(firstIndex + i).getToken().equals("{"))
            .mapToObj( i -> createDecoratorRange(decorationLabels[i], lineNumber, tokens.get(firstIndex + i).getStart()))
            .toList();
    }

    /**
     * Generates text decorations for a specific type of relationship within a given line.
     *
     * @param line the text line containing the relationship definition
     * @param lineNumber the line number in the document where the relationship is defined
     * @param decorationLabels an array of decoration labels to be applied to the relationship
     * @return a list of {@link DecoratorRange} objects representing the decorations for the relationship
     */
    List<DecoratorRange> decorationsForElement(String line, int lineNumber, String[] decorationLabels) {

        List<LineToken> tokens = tokenizer.tokenize(line);
        int firstIndex = (tokens.get(1).getToken().equals(LineTokenizer.TOKEN_EXPR_ASSIGNMENT)) ? 3 : 1;

        return IntStream.range(0, decorationLabels.length)
            .filter( i -> firstIndex + i < tokens.size() && !tokens.get(firstIndex + i).getToken().equals("{"))
            .mapToObj( i -> createDecoratorRange(decorationLabels[i], lineNumber, tokens.get(firstIndex + i).getStart()))
            .toList();
    }

    private final static String[] PERSON_DECORATIONS = new String[] {"name: ", "description: ", "tags: "};
    List<DecoratorRange> decorationsForPerson(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, PERSON_DECORATIONS);
    }

    private final static String[] SOFTWARE_SYSTEM_DECORATIONS = new String[] {"name: ", "description: ", "tags: "};
    List<DecoratorRange> decorationsForSoftwareSystem(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, SOFTWARE_SYSTEM_DECORATIONS);
    }

    private final static String[] CONTAINER_DECORATIONS = new String[] {"name: ", "description: ", "technology: ", "tags: "};
    List<DecoratorRange> decorationsForContainer(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, CONTAINER_DECORATIONS);
    }

    private final static String[] COMPONENT_DECORATIONS = new String[] {"name: ", "description: ", "technology: ", "tags: "};
    List<DecoratorRange> decorationsForComponent(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, COMPONENT_DECORATIONS);
    }

    private final static String[] DEPLOYMENT_NODE_DECORATIONS = new String[] {"name: ", "description: ", "technology: ", "tags: ", "instances: "};
    List<DecoratorRange> decorationsForDeploymentNode(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, DEPLOYMENT_NODE_DECORATIONS);
    }

    private final static String[] INFRASTRUCUTRE_NODE_DECORATIONS = new String[] {"name: ", "description: ", "technology: ", "tags: "};
    List<DecoratorRange> decorationsForInfrastructureNode(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, INFRASTRUCUTRE_NODE_DECORATIONS);
    }

    private final static String[] SOFTWARE_SYSTEM_INSTANCE_DECORATIONS = new String[] {"identifier: ", "deploymentGroups: ", "tags: "};
    List<DecoratorRange> decorationsForSoftwareSystemInstance(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, SOFTWARE_SYSTEM_INSTANCE_DECORATIONS);
    }

    private final static String[] CONTAINER_INSTANCE_DECORATIONS = new String[] {"identifier: ", "deploymentGroups: ", "tags: "};
    List<DecoratorRange> decorationsForContainerInstance(String line, int lineNumber) {
        return decorationsForElement(line, lineNumber, CONTAINER_INSTANCE_DECORATIONS);
    }

    private final static String[] RELATIONSHIP_DECORATIONS = new String[] {"description: ", "technology: ", "tags: "};

    /**
     * Calculates text decorations for a given element in the document.
     * 
     * @param elementWithContext the {@link C4ObjectWithContext} containing the element to decorate
     * @param line the text line containing the element definition
     * @param lineNumber the line number in the document where the element is defined
     * @return a list of {@link DecoratorRange} objects representing the text decorations for the element
     */
    public List<DecoratorRange> calculateDecoratorsForElement(C4ObjectWithContext<Element> elementWithContext, String line, int lineNumber) {
        
        List<DecoratorRange> decorations = new ArrayList<>();

        Element context = elementWithContext.getObject();
        
        if(context instanceof Person) {
            decorations = decorationsForPerson(line, lineNumber);
        }
        else if(context instanceof SoftwareSystem) {
            decorations = decorationsForSoftwareSystem(line, lineNumber);
        }

        else if(context instanceof Container) {
            decorations = decorationsForContainer(line, lineNumber);
        }

        else if(context instanceof Component) {
            decorations = decorationsForComponent(line, lineNumber);
        }

        else if(context instanceof DeploymentNode) {
            decorations = decorationsForDeploymentNode(line, lineNumber);
        }

        else if(context instanceof InfrastructureNode) {
            decorations = decorationsForInfrastructureNode(line, lineNumber);
        }

        else if(context instanceof SoftwareSystemInstance) {
            decorations = decorationsForSoftwareSystemInstance(line, lineNumber);
        }

        else if(context instanceof ContainerInstance) {
            decorations = decorationsForContainerInstance(line, lineNumber);
        }

        return decorations;
    }
     
    /**
     * Creates a {@link DecoratorRange} object representing a single decoration for an element or relationship.
     * 
     * @param type the type of the decoration (e.g., "name: ", "description: ")
     * @param line the line number where the decoration is to be applied
     * @param character the character position in the line where the decoration starts
     * @return a {@link DecoratorRange} object representing the decoration range
     */
    private DecoratorRange createDecoratorRange(String type, int line, int character) {
       return new DecoratorRange(type ,new Range(new Position(line, character), new Position(line, character)));
    }

    /**
     * Adjusts the line number from a zero-based index.
     *
     * @param key the original line number (1-based index)
     * @return the adjusted line number (0-based index)
     */
    private int lineNumber(Integer key) {
        return key-1;
    }
 
     /**
     * Represents a range of text to be decorated in a C4 DSL document.
     */
    @Data
    public class DecoratorRange {
        
        private String type;        
        private Range range;

        /**
         * Constructs a new {@code DecoratorRange} with the specified type and range.
         *
         * @param type the type of the decoration (e.g., "name: ", "description: ")
         * @param range the range within the document where the decoration is applied
         */        
        public DecoratorRange(String type, Range range) {
            this.type = type;
            this.range = range;        
        }

    }
    
}

