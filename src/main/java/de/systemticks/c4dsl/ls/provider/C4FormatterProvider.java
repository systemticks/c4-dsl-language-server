package de.systemticks.c4dsl.ls.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.TextEdit;

import de.systemticks.c4dsl.ls.model.C4CompletionScope;
import de.systemticks.c4dsl.ls.model.C4DocumentModel;
import de.systemticks.c4dsl.ls.utils.C4Utils;
import de.systemticks.c4dsl.ls.utils.LineToken;
import de.systemticks.c4dsl.ls.utils.LineTokenizer;

/**
 * Provides text formatting functionalities for a C4 DSL document in the context
 * of the Language Server Protocol (LSP).
 * 
 * This provider is responsible for calculating text edits required to properly
 * format a document according to a defined indentation style and token spacing rules.
 */
public class C4FormatterProvider {

    private static final Pattern COMMENT_PATTERN = Pattern.compile("^\\s*?(//|#).*$");

    private int indentPerScope;
    private LineTokenizer lineTokenizer;

    /**
     * Constructs a new {@code C4FormatterProvider} with the specified indentation size.
     *
     * @param indentPerScope the number of spaces per indentation level
     */
    public C4FormatterProvider(int indentPerScope) {
        this.indentPerScope = indentPerScope;
        this.lineTokenizer = new LineTokenizer();
    }

    /**
     * Updates the indentation size per scope level.
     *
     * @param newIndent the new indentation size
     */
    public void updateIndent(int newIndent) {
        this.indentPerScope = newIndent;
    }

    /**
     * Calculates the text edits required to format the given C4 document model.
     *
     * This method analyzes each line of the document, checks for proper indentation
     * and token spacing, and generates the necessary {@link TextEdit} objects
     * to correct any formatting issues.
     *
     * @param model the C4 document model representing the current document state
     * @return a list of {@link TextEdit} objects representing the required formatting changes
     */
    public List<TextEdit> calculateFormattedTextEdits(C4DocumentModel model) {

        List<TextEdit> result = new ArrayList<>();
        List<String> rawLines = model.getRawLines();

        for(int lineIdx=0; lineIdx<rawLines.size(); lineIdx++) {
            var currentIdx = lineIdx;
            model.getNearestScope(currentIdx).ifPresent( scope -> {
                String originText = rawLines.get(currentIdx);
                int expectedIndentDepth = getExpectedIndentDepth(scope, currentIdx+1);
                int firstNonWhiteSpace = C4Utils.findFirstNonWhitespace(originText, 0, true);
                if(!originText.isBlank() && (expectedIndentDepth != firstNonWhiteSpace || hasWhitespacesBetweenTokens(originText))) {
                    String newText = createNewText(originText, expectedIndentDepth);
                    result.add( createTextEdit(newText, originText, currentIdx));
                }    
            });
        }

        return result;
    }
   
    /**
     * Calculates the expected indentation depth for a given scope and line index.
     *
     * @param scope the completion scope defining the current context
     * @param currentIdx the current line index in the document
     * @return the expected number of leading whitespace characters for the line
     */
    int getExpectedIndentDepth(C4CompletionScope scope, int currentIdx) {
        return (scope.getStartsAt() == currentIdx || scope.getEndsAt() == currentIdx ? scope.getDepth() : scope.getDepth()+1) * indentPerScope;
    }
    
    /**
     * Creates a new formatted text line by adjusting the leading whitespace and removing
     * unnecessary spaces between tokens.
     *
     * @param oldText the original text of the line
     * @param leadingWhiteSpaces the number of leading whitespace characters required
     * @return the new formatted text
     */
    String createNewText(String oldText, int leadingWhiteSpaces) {
        return (new String(" ")).repeat(leadingWhiteSpaces) + removeWhiteSpacesBetweenTokens(oldText.trim());
    }


    /**
     * Creates a {@link TextEdit} object representing the formatting changes needed
     * for a specific line in the document.
     *
     * @param newText the newly formatted text for the line
     * @param oldText the original text of the line
     * @param line the line number in the document
     * @return a {@link TextEdit} object representing the change
     */
    TextEdit createTextEdit(String newText, String oldText, int line) {
        var range = new Range( new Position(line, 0), new Position(line, oldText.length()));
        return new TextEdit(range, newText);
    }

    /**
     * Checks if there are extra whitespaces between tokens in the given text line.
     *
     * @param text the text line to check
     * @return {@code true} if extra whitespaces are found between tokens; {@code false} otherwise
     */
    boolean hasWhitespacesBetweenTokens(String text) {
        List<LineToken> tokens = lineTokenizer.tokenize(text);
        if(tokens.size() > 1) {
            int index = 0;
            while(index < tokens.size()-1) {
                if(tokens.get(index+1).getStart() - tokens.get(index).getEnd() > 1) {
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    /**
     * Removes unnecessary whitespaces between tokens in the given text line.
     *
     * If the line is a single-line comment, the text is returned unchanged.
     *
     * @param text the text line to process
     * @return the text line with unnecessary whitespaces removed between tokens
     */
    String removeWhiteSpacesBetweenTokens(String text) {

        if(isSingleLineComment(text)) {
            return text;
        }

        List<LineToken> tokens = lineTokenizer.tokenize(text);
        if(tokens.size() < 2) {
            return text;
        }

        return tokens.stream().map(LineToken::getToken).collect(Collectors.joining(" "));
    }

    /**
     * Determines if the given text line is a single-line comment.
     *
     * @param text the text line to check
     * @return {@code true} if the line is a single-line comment; {@code false} otherwise
     */
    boolean isSingleLineComment(String text) {
        return COMMENT_PATTERN.matcher(text).matches();
    }
}
