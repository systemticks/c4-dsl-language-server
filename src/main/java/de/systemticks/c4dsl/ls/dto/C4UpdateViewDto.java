package de.systemticks.c4dsl.ls.dto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import lombok.Data;

@Data
public class C4UpdateViewDto {
    
    private static final Gson GSON = new Gson();

    private String document;
    private String viewKey;
    private String renderer;

    public static C4UpdateViewDto fromJson(JsonObject jsonObject) {
        return GSON.fromJson(jsonObject, C4UpdateViewDto.class);
    }
}
