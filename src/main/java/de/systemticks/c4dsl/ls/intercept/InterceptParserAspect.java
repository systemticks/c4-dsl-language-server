package de.systemticks.c4dsl.ls.intercept;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.structurizr.model.Element;
import com.structurizr.model.Relationship;
import com.structurizr.view.ModelView;

@Aspect
public class InterceptParserAspect {

    @Inject
    StructurizrDslParserListener parserListener;
    
    private static final Logger logger = LoggerFactory.getLogger(InterceptParserAspect.class);

    private int currentLineNumber;
    private File currentDslFile;
    private String currentIdentifier;

    // startContext
    @Pointcut("target(com.structurizr.dsl.StructurizrDslParser) && execution(* startContext(..))")
    public void interceptStartContext() { }

    @After("interceptStartContext()")
    public void interceptStartContextAdvice(JoinPoint joinPoint) throws Exception {
        getContext(joinPoint).ifPresent( context -> {
            parserListener.onStartContext(currentDslFile, currentLineNumber, context.hashCode(), context.getClass().getSimpleName());
        });
    }

    private Optional<Object> getContext(JoinPoint joinPoint) throws Exception {

        try {
            Field field;
            field = joinPoint.getThis().getClass().getDeclaredField("contextStack");
            field.setAccessible(true);
            Stack<?> contextStack = (Stack<?>) field.get(joinPoint.getThis());
            if(contextStack != null && !contextStack.empty()) {
                return Optional.of(contextStack.peek());
            }
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            logger.error("Cannot determine context for {}: {}", joinPoint.toLongString(), e.getMessage());
            parserListener.onException(new RuntimeException(e.getMessage()));
        }

        return Optional.empty();
    }

    // endContext
    @Pointcut("target(com.structurizr.dsl.StructurizrDslParser) && execution(* endContext(..))")
    public void interceptEndContext() { }

    @Before("interceptEndContext()")
    public void interceptEndContextAdvice(JoinPoint joinPoint) throws Exception {
        getContext(joinPoint).ifPresent( context -> {
            parserListener.onEndContext(currentDslFile, currentLineNumber, context.hashCode(), context.getClass().getSimpleName());
        });
    }

    // parseLineAt
    @Pointcut("target(com.structurizr.dsl.StructurizrDslParser) && execution(* parseLineAt(..))")
    public void interceptParseLineAt() { }

    @After("interceptParseLineAt()")
    public void interceptParseLineAtAdvice(JoinPoint joinPoint) {
        currentLineNumber = (int) joinPoint.getArgs()[0];
        currentDslFile = (File) joinPoint.getArgs()[1];
    }

    // *View.parse
    @Pointcut("target(com.structurizr.dsl.AbstractViewParser) && execution(com.structurizr.view.ModelView+ parse(..))")
    public void interceptParsedView() { }
 
    @Around("interceptParsedView()")
    public Object interceptParsedViewAdvice(ProceedingJoinPoint joinPoint) throws Exception {

        ModelView view = null;

        try {
            view = (ModelView) joinPoint.proceed();
            parserListener.onParsedView(currentDslFile, currentLineNumber, view);
        } catch (Throwable e) {
            logger.error("Cannot intercept parsed View for {}: {}", joinPoint.toLongString(), e.getMessage());
            parserListener.onException(new RuntimeException(e.getMessage()));
        }

        return view;
    }

    // registerIdentifier
    @Pointcut("target(com.structurizr.dsl.IdentifiersRegister) && execution(* validateIdentifierName(..))")
    public void interceptRegisterIdentifier() { }

    @After("interceptRegisterIdentifier()")
    public void interceptRegisterIdentifierAdvice(JoinPoint joinPoint) {
        currentIdentifier = (String)joinPoint.getArgs()[0];
    }

    // Model.parse
    @Pointcut("target(com.structurizr.dsl.AbstractParser) && execution(com.structurizr.model.Element+ parse(..))")
    public void interceptParsedModel() { }

    @Around("interceptParsedModel()")
    public Object interceptParsedModelAdvice(ProceedingJoinPoint joinPoint) throws Exception {

        Element element = null;

        try {
            element = (Element) joinPoint.proceed();
            parserListener.onParsedModelElement(currentDslFile, currentLineNumber, currentIdentifier, element);
            currentIdentifier = null;
        } catch (Throwable e) {
            logger.error("Cannot intercept parsed Model for {}: {}", joinPoint.toLongString(), e.getMessage());
            parserListener.onException(new RuntimeException(e.getMessage()));
        }

        return element;
    }

    // RelationshipParser
    @Pointcut("target(com.structurizr.dsl.AbstractRelationshipParser) && execution(com.structurizr.model.Relationship parse(..))")
    public void interceptParsedRelationship() { }

    @Around("interceptParsedRelationship()")
    public Object interceptParsedRelationshipAdvice(ProceedingJoinPoint joinPoint) throws Exception {

        Relationship relationship = null;

        try {
            relationship = (Relationship) joinPoint.proceed();
            parserListener.onParsedRelationShip(currentDslFile, currentLineNumber, currentIdentifier, relationship);
            currentIdentifier = null;
        } catch (Throwable e) {
            logger.error("Cannot intercept parsed Relationship for {}: {}", joinPoint.toLongString(), e.getMessage());
            parserListener.onException(new RuntimeException(e.getMessage()));
        }

        return relationship;
    }

    // all kind of color parsing 
    @Pointcut("execution(* com.structurizr.dsl.ElementStyleParser.parseColour(..)) || "+
              "execution(* com.structurizr.dsl.ElementStyleParser.parseBackground(..)) || "+
              "execution(* com.structurizr.dsl.ElementStyleParser.parseStroke(..)) || "+
              "execution(* com.structurizr.dsl.RelationshipStyleParser.parseColour(..)) ")
    public void interceptParsedColor() { }

    @After("interceptParsedColor()")
    public void interceptParsedColorAdvice(JoinPoint joinPoint) {
        parserListener.onParsedColor(currentDslFile, currentLineNumber);
    }

    // Scripting
    @Pointcut("target(com.structurizr.dsl.ScriptDslContext) && execution(* run(..))")
    public void interceptScriptRunner() { }

    @Around("interceptScriptRunner()")
    @SuppressWarnings("unchecked")   
    public Object interceptScriptRunnerAdvice(ProceedingJoinPoint joinPoint) {
        logger.info("Scripting Block is ignored");
        switch (joinPoint.getArgs().length) {
            case 2:
                parserListener.onRunExternalScript((File)joinPoint.getArgs()[1]);
                break;
            case 3:
                parserListener.onRunInlineScript((String)joinPoint.getArgs()[1], (List<String>) joinPoint.getArgs()[2]);
                break;
            default:
                logger.warn("Cannot find an advice with {} arguments", joinPoint.getArgs().length);
        }
        return null;
    }

    // include
    @Pointcut("target(com.structurizr.dsl.IncludeParser) && execution(* parse(..))")
    public void interceptParsedInclude() { }

    @After("interceptParsedInclude()")
    public void interceptParsedIncludeAdvice(JoinPoint joinPoint) {

        getFile(joinPoint.getArgs()[0]).ifPresent( referencedFile -> {
            getPath(joinPoint.getArgs()[1]).ifPresent( path -> {
                parserListener.onInclude(currentDslFile, currentLineNumber, referencedFile, path);
            });
        });
        
    }

    private Optional<File> getFile(Object contextObj) {
        try {
            Method getFiles = contextObj.getClass().getDeclaredMethod("getFiles");
            getFiles.setAccessible(true);
            List<?> includeFiles = (List<?>)getFiles.invoke(contextObj);
            Object includedFile = includeFiles.get(0);
            Method getFile = includedFile.getClass().getDeclaredMethod("getFile");
            getFile.setAccessible(true);
            return Optional.of((File)getFile.invoke(includedFile));
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            logger.error("Cannot determine File onInclude {}", e.getMessage());
        }
        return Optional.empty();
    }

    private Optional<String> getPath(Object tokenObject) {
        try {
            Method get = tokenObject.getClass().getDeclaredMethod("get", int.class);
            get.setAccessible(true);
            return Optional.of((String)get.invoke(tokenObject, 1));
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            logger.error("Cannot determine Path onInclude {}", e.getMessage());
        }
        return Optional.empty();
    }

}
